<?php

namespace Drupal\webform_inline_entity_form\Element;

use Drupal\Core\Render\Element\FormElement;
use Drupal\inline_entity_form\Element\InlineEntityForm;

/**
 * Provides a 'webform_inline_entity_form'.
 *
 * Webform elements are just wrappers around form elements, therefore every
 * webform element must have correspond FormElement.
 *
 * Below is the definition for a custom 'webform_example_element' which just
 * renders a simple text field.
 *
 * @FormElement("webform_inline_entity_form")
 *
 * @see \Drupal\Core\Render\Element\FormElement
 * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Render%21Element%21FormElement.php/class/FormElement
 * @see \Drupal\Core\Render\Element\RenderElement
 * @see https://api.drupal.org/api/drupal/namespace/Drupal%21Core%21Render%21Element
 * @see \Drupal\webform_inline_entity_form\Element\WebformInlineEntityForm
 */
class WebformInlineEntityForm extends InlineEntityForm {

}
