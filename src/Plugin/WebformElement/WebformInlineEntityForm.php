<?php

namespace Drupal\webform_inline_entity_form\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\webform\Annotation\WebformElement;
use Drupal\webform\Plugin\WebformElement\WebformEntityReferenceTrait;
use Drupal\webform\Plugin\WebformElementBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a 'webform_example_element' element.
 *
 * @WebformElement(
 *   id = "webform_inline_entity_form",
 *   label = @Translation("Inline entity form"),
 *   description = @Translation("Allows creating or editing
 *   an entity as part of form submission, saving the entity reference."),
 *   category = @Translation("Entity reference elements"),
 * )
 *
 * @see \Drupal\webform_inline_entity_form\Element\WebformInlineEntityForm
 * @see \Drupal\webform\Plugin\WebformElementBase
 * @see \Drupal\webform\Plugin\WebformElementInterface
 * @see \Drupal\webform\Annotation\WebformElement
 */
class WebformInlineEntityForm extends WebformElementBase {

  use WebformEntityReferenceTrait {
    form as protected traitForm;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    return [
        // Entity reference settings.
        'target_type' => '',
        'selection_handler' => 'default',
        'selection_settings' => [],

        // Settings for form:
        'load_existing' => 1,
        'owner_current' => 1,
        'form_mode' => 'default',

        // IEF fields
        'entity_type' => 'default',
        'bundle' => 'default',
      ] + parent::defineDefaultProperties()
      + $this->defineDefaultMultipleProperties();
  }

  /* ************************************************************************ */

  /**
   * {@inheritdoc}
   */
  public function setDefaultValue(array &$element) {
    $entityManager = \Drupal::entityTypeManager();
    $entityTypeId = $element['#target_type'] ?? NULL;

    if (!$entityTypeId) {
      return;
    }

    // If value set, great:
    if (isset($element['#default_value'])) {
      $id = $element['#default_value'];
    }
    else {
      // Load individual:
      $loadExisting = !isset($element['#load_existing']) || $element['#load_existing'] == 1;
      if ($loadExisting) {
        $entity_type = $entityManager->getDefinition($entityTypeId);
        $query = \Drupal::entityQuery($entityTypeId)
          ->condition($entity_type->getKey('bundle'), $element['#bundle']);

        $ownedByCurrent = !isset($element['#owner_current']) || $element['#owner_current'] == 1;
        if ($ownedByCurrent) {
          $owned = is_subclass_of($entity_type->getClass(), EntityOwnerInterface::class) && $entity_type->hasKey('owner');
          if ($owned) {
            $query->condition($entity_type->getKey('owner'), \Drupal::currentUser()
              ->id());
          }
        }

        if ($sortField = $element['#selection_settings']['sort']['field']) {
          if ($sortField != '_none') {
            $query->sort($sortField, $element['#selection_settings']['sort']['direction']);
          }
        }

        $result = $query->execute();

        $id = reset($result);
      }
    }

    // Load entity
    if ($id) {
      $storage = $entityManager->getStorage($entityTypeId);
      $entity = $storage->load($id);
      if ($entity) {
        $element['#default_value'] = $entity;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function supportsMultipleValues() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function hasMultipleWrapper() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Generally, elements will not be processing any submitted properties.
    // It is possible that a custom element might need to call a third-party API
    // to 'register' the element.
    $form_state->setValue('entity_type', $form_state->getValue('target_type'));

    if ($target_bundles = $form_state->getValue('selection_settings')['target_bundles']) {
      $form_state->setValue('bundle', reset($target_bundles));
    }
    else {
      $form_state->setValue('bundle', 'default');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function hasMultipleValues(array $element) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getExportDefaultOptions() {
    return [
      'entity_reference_items' => ['id', 'title', 'url'],
    ];
  }

  /**
   * {@inheritdoc}
   *
   * This is the configuration form for the element.
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);
    $form = $this->traitForm($form, $form_state);

    $form['element']['load_existing'] = [
      '#title' => t('Load existing entity if found'),
      '#type' => 'select',
      '#options' => [
        1 => t('Yes'),
        0 => t('No (new entity will be created for each submission)'),
      ],
      '#description' => t('If this is checked and an entity is not found, a new entity will be created for the first submission.'),
    ];

    $form['element']['owner_current'] = [
      '#title' => t('Owner is logged in user'),
      '#type' => 'select',
      '#options' => [
        1 => t('Yes'),
        0 => t('No'),
      ],
    ];

    $element_properties = $form_state->get('element_properties');
    if ($element_properties && $entityType = $element_properties['target_type']) {
      $options = \Drupal::service('entity_display.repository')
        ->getFormModeOptions($entityType);

      $form['element']['form_mode'] = [
        '#type' => 'select',
        '#title' => t('Form mode'),
        '#options' => $options,
      ];
    }

    if (isset($form['entity_reference']['selection_settings']['target_bundles'])) {
      $form['entity_reference']['selection_settings']['target_bundles']['#multiple'] = FALSE;
      //      $form['entity_reference']['selection_settings']['target_bundles']['#type'] = 'select';
    }

    return $form;
  }

  // ON THE WEBFORM ITSELF:

  /**
   * {@inheritdoc}
   */
  protected function prepareElementValidateCallbacks(array &$element, WebformSubmissionInterface $webform_submission = NULL) {
    parent::prepareElementValidateCallbacks($element, $webform_submission);
    $element['#element_validate'][] = [
      get_class($this),
      'validateInlineEntityForm',
    ];
  }

  /**
   * Form API callback. Remove target id property and create an array of entity
   * ids.
   */
  public static function validateInlineEntityForm(array &$element, FormStateInterface $form_state) {
    // Must use ::getValue($element['#parents']) because $element['#value'] is
    // not being updated.
    // @see \Drupal\Core\Entity\Element\EntityAutocomplete::validateEntityAutocomplete
    $value = $form_state->getValue($element['#parents']);
    if (empty($value) || !is_array($value)) {
      return;
    }

    if (isset($element['#entity'])) {
      $entity = $element['#entity'];
      $entity->save();
      $form_state->setValueForElement($element, $entity->id());
    }
  }

}
