# Webform Inline Entity Form

This module provides an element type that can be added to a webform
that embeds an entity form into the webform, saves/updates the entity
with the data on form submission, and provides an entity reference as 
the element value in the submission. 

Using the entity reference settings, you can select what type of 
entity and what bundle the entity should be.

You can also choose to have the element load the first available existing 
entity (based on your sort settings), optionally also only those owned by 
the user submitting the form.

Note: users must have access/permissions to create and update entities
or the element will not work.

- For a full description of the module, visit the [project page].
- To submit bug reports and feature suggestions, or to track changes, use the
  [issue queue].

[Project page]: https://www.drupal.org/project/webform_inline_entity_form
[issue queue]: https://www.drupal.org/project/issues/webform_inline_entity_form

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

Webform and Inline Entity Form modules.

## Installation

- Install as you would normally install a contributed Drupal module. For further
  information, see _[Installing Drupal Modules]_.

[Installing Drupal Modules]: https://www.drupal.org/docs/extending-drupal/installing-drupal-modules

## Configuration

1. Add element of type "Inline entity form".
2. Enter title.
3. If you want to create a new entity each submission, change "Load existing entity if found" to "No".
4. If you want to load the entity for that user (e.g. a node created by the user, a Profile entity owned by the user), then leave "Owned by logged in user" as "Yes."
5. Change the form mode if you would like. You will need to have saved the element first to load the form modes for the selected entity type.
6. Select the entity type under "Type of item to reference".
7. Select the reference method "Default".
8. Select the bundle type, e.g. content type.
9. Select sort settings if there would be multiple existing entities you want to load; it will pick the first one, so you can sort accordingly.
10. Form will then show the entity form and load an existing entity if set up and available.

## Maintainers


### Current maintainers

- [Maxwell Keeble](https://www.drupal.org/u/maxwellkeeble)
